package com.xu.context;

import com.xu.service.IMessageService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;

@Component
public class MessageContext {

    private static final Logger LOGGER = LoggerFactory.getLogger(MessageContext.class);

    private Map<String, IMessageService<String>> handlers = new HashMap<>();

    public void register(String type, IMessageService<String> handler) {
        LOGGER.info("================6.3 code和service以键值对的方式放入map中========================");
        handlers.put(type, handler);
    }

    public IMessageService<String> get(String type) {
        LOGGER.info("================16. 根据code获取相应的Service========================");
        IMessageService<String> handler = handlers.get(type);
        return handler;
    }

}
