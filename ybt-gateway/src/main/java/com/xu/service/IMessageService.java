package com.xu.service;


public interface IMessageService<String> {

    Object handle(String requestId, String message);

    String getCode();

}
