package com.ebiz.demo;

import com.ebiz.demo.dto.MainDTO;
import com.ebiz.demo.dto.MainDTO.*;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

//@SpringBootApplication
//@ComponentScan("com.ebiz.gateway")
//@RunWith(SpringRunner.class)
//@SpringBootTest
public class GatewayApplicationTests {


    @Test
    public void contextLoads() throws Exception {
        
        MainDTO dto = new MainDTO();
        
        List<InsuYearFlag> insuYearFlags = new ArrayList<>();

        List<InsuYear> insuYears = new ArrayList<>();

        List<PayEndYearFlag> payEndYearFlags = new ArrayList<>();

        List<PayIntv> payIntvs = new ArrayList<>();

        List<Nationality> nationalitys = new ArrayList<>();

        List<IDType> iDTypes = new ArrayList<>();

        List<Sex> sexs = new ArrayList<>();
        
        InsuYearFlag insuYearFlag = new InsuYearFlag();
        insuYearFlag.setOrig("06");
        insuYearFlag.setDest("1");
        insuYearFlags.add(insuYearFlag);
        
        InsuYearFlag insuYearFlag1 = new InsuYearFlag();
        insuYearFlag1.setOrig("01");
        insuYearFlag1.setDest("2");
        insuYearFlags.add(insuYearFlag1);
        dto.setInsuYearFlag(insuYearFlags);
        
        InsuYear InsuYear = new InsuYear();
        InsuYear.setOrig("999");
        InsuYear.setDest("1000");
        insuYears.add(InsuYear);
        dto.setInsuYear(insuYears);
        
        PayEndYearFlag PayEndYearFlag = new PayEndYearFlag();
        PayEndYearFlag.setOrig("02");
        PayEndYearFlag.setDest("2");
        payEndYearFlags.add(PayEndYearFlag);
        
        PayEndYearFlag PayEndYearFlag1 = new PayEndYearFlag();
        PayEndYearFlag1.setOrig("01");
        PayEndYearFlag1.setDest("1");
        payEndYearFlags.add(PayEndYearFlag1);
        dto.setPayEndYearFlag(payEndYearFlags);
        
        PayIntv PayIntv = new PayIntv();
        PayIntv.setOrig("01");
        PayIntv.setDest("5");
        payIntvs.add(PayIntv);
        
        PayIntv PayIntv1 = new PayIntv();
        PayIntv1.setOrig("02");
        PayIntv1.setDest("1");
        payIntvs.add(PayIntv1);
        dto.setPayIntv(payIntvs);
        
        
        Nationality Nationality = new Nationality();
        Nationality.setOrig("CN");
        Nationality.setDest("CHN");
        nationalitys.add(Nationality);
        dto.setNationality(nationalitys);
        
        IDType IDType = new IDType();
        IDType.setOrig("01");
        IDType.setDest("1");
        iDTypes.add(IDType);
        dto.setiDType(iDTypes);
        
        Sex sex = new Sex();
        sex.setOrig("0");
        sex.setDest("F");
        sexs.add(sex);

        Sex sex1 = new Sex();
        sex1.setOrig("1");
        sex1.setDest("M");
        sexs.add(sex1);
        dto.setSex(sexs);
        
        //System.out.println(JsonUtil.writeValue(dto));
        
    }

}
