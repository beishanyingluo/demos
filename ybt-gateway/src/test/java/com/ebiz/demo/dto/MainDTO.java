package com.ebiz.demo.dto;

import java.util.List;

public class MainDTO {

    private List<InsuYearFlag> insuYearFlag;

    private List<InsuYear> insuYear;

    private List<PayEndYearFlag> payEndYearFlag;

    private List<PayIntv> payIntv;

    private List<Nationality> nationality;

    private List<IDType> iDType;

    private List<Sex> sex;

    public List<InsuYearFlag> getInsuYearFlag() {
        return insuYearFlag;
    }

    public void setInsuYearFlag(List<InsuYearFlag> insuYearFlag) {
        this.insuYearFlag = insuYearFlag;
    }

    public List<InsuYear> getInsuYear() {
        return insuYear;
    }

    public void setInsuYear(List<InsuYear> insuYear) {
        this.insuYear = insuYear;
    }

    public List<PayEndYearFlag> getPayEndYearFlag() {
        return payEndYearFlag;
    }

    public void setPayEndYearFlag(List<PayEndYearFlag> payEndYearFlag) {
        this.payEndYearFlag = payEndYearFlag;
    }

    public List<PayIntv> getPayIntv() {
        return payIntv;
    }

    public void setPayIntv(List<PayIntv> payIntv) {
        this.payIntv = payIntv;
    }

    public List<Nationality> getNationality() {
        return nationality;
    }

    public void setNationality(List<Nationality> nationality) {
        this.nationality = nationality;
    }

    public List<IDType> getiDType() {
        return iDType;
    }

    public void setiDType(List<IDType> iDType) {
        this.iDType = iDType;
    }

    public List<Sex> getSex() {
        return sex;
    }

    public void setSex(List<Sex> sex) {
        this.sex = sex;
    }

    public static class InsuYearFlag {

        private String orig;

        private String dest;

        public String getOrig() {
            return orig;
        }

        public void setOrig(String orig) {
            this.orig = orig;
        }

        public String getDest() {
            return dest;
        }

        public void setDest(String dest) {
            this.dest = dest;
        }
    }

    public static class InsuYear {

        private String orig;

        private String dest;

        public String getOrig() {
            return orig;
        }

        public void setOrig(String orig) {
            this.orig = orig;
        }

        public String getDest() {
            return dest;
        }

        public void setDest(String dest) {
            this.dest = dest;
        }
    }

    public static class PayEndYearFlag {

        private String orig;

        private String dest;

        public String getOrig() {
            return orig;
        }

        public void setOrig(String orig) {
            this.orig = orig;
        }

        public String getDest() {
            return dest;
        }

        public void setDest(String dest) {
            this.dest = dest;
        }
    }

    public static class PayIntv {

        private String orig;

        private String dest;

        public String getOrig() {
            return orig;
        }

        public void setOrig(String orig) {
            this.orig = orig;
        }

        public String getDest() {
            return dest;
        }

        public void setDest(String dest) {
            this.dest = dest;
        }
    }

    public static class Nationality {

        private String orig;

        private String dest;

        public String getOrig() {
            return orig;
        }

        public void setOrig(String orig) {
            this.orig = orig;
        }

        public String getDest() {
            return dest;
        }

        public void setDest(String dest) {
            this.dest = dest;
        }
    }

    public static class IDType {

        private String orig;

        private String dest;

        public String getOrig() {
            return orig;
        }

        public void setOrig(String orig) {
            this.orig = orig;
        }

        public String getDest() {
            return dest;
        }

        public void setDest(String dest) {
            this.dest = dest;
        }
    }

    public static class Sex {

        private String orig;

        private String dest;

        public String getOrig() {
            return orig;
        }

        public void setOrig(String orig) {
            this.orig = orig;
        }

        public String getDest() {
            return dest;
        }

        public void setDest(String dest) {
            this.dest = dest;
        }
    }

}
